CREATE UNIQUE INDEX users_uniq_login
  ON users (login(256));
CREATE UNIQUE INDEX users_uniq_email
  ON users (email(512));
CREATE UNIQUE INDEX session_uniq_hash
  ON sessions (hashkey(60));