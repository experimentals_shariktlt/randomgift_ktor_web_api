package giftrandom.dto

data class Pack(var id: Int, var name: String, var descriptions: String, var image: String, var price: Long, var priceUnwrap: Long, var priceSell: Long)