package giftrandom.dto

data class GiftPackage(val id: Int, val pack: Pack, var gift: Gift?, var status: String) {
    //constructor(PackDAO)
}