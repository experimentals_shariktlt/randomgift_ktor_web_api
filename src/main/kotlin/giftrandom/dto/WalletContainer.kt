package giftrandom.dto

data class WalletContainer(var success: Boolean, val body: Any?, var balance: Long) {
    constructor(balance: Long, body: Any?) : this(true, body, balance)
}