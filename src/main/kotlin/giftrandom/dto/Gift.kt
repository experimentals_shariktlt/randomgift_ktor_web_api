package giftrandom.dto

data class Gift(var id: Int, var name: String, var image: String, var price: Long)
