package giftrandom.utils

import org.mindrot.jbcrypt.BCrypt


class Hashing(private val logRounds: Int) {

    fun hash(password: String): String {
        return BCrypt.hashpw(password, BCrypt.gensalt(logRounds))
    }

    fun verifyHash(password: String, hash: String): Boolean {
        return BCrypt.checkpw(password, hash)
    }
//
//    fun verifyAndUpdateHash(password: String, hash: String, updateFunc: Function<Boolean>): Boolean {
//        if (BCrypt.checkpw(password, hash)) {
//            val rounds = getRounds(hash)
//            // It might be smart to only allow increasing the rounds.
//            // If someone makes a mistake the ability to undo it would be nice though.
//            if (rounds != logRounds) {
//                log.debug("Updating password from {} rounds to {}", rounds, logRounds)
//                val newHash = hash(password)
//                return updateFunc(newHash)
//            }
//            return true
//        }
//        return false
//    }
//
//    /*
//     * Copy pasted from BCrypt internals :(. Ideally a method
//     * to exports parts would be public. We only care about rounds
//     * currently.
//     */
//    private fun getRounds(salt: String): Int {
//        var minor = 0.toChar()
//        var off = 0
//
//        if (salt[0] != '$' || salt[1] != '2')
//            throw IllegalArgumentException("Invalid salt version")
//        if (salt[2] == '$')
//            off = 3
//        else {
//            minor = salt[2]
//            if (minor != 'a' || salt[3] != '$')
//                throw IllegalArgumentException("Invalid salt revision")
//            off = 4
//        }
//
//        // Extract number of rounds
//        if (salt[off + 2] > '$')
//            throw IllegalArgumentException("Missing salt rounds")
//        return Integer.parseInt(salt.substring(off, off + 2))
//    }

//    companion object {
//        private val log = LoggerFactory.getLogger(UpdatableBCrypt::class.java)
//    }
}