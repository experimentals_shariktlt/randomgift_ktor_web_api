package giftrandom

import giftrandom.api.*
import giftrandom.db.SessionToken
import giftrandom.db.connectDb
import giftrandom.db.initDbSchema
import giftrandom.features.SessionCache
import giftrandom.services.jsonAthentification
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.authentication
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType
import io.ktor.routing.Routing
import io.ktor.routing.route
import io.ktor.sessions.Sessions
import io.ktor.sessions.cookie
import io.ktor.util.hex
import java.text.DateFormat
import java.time.Duration

val DB_COLLATE:String = "utf8_unicode_ci"
val cookieDuration = Duration.ofDays(365)

fun Application.randomGiftApplication() {

    val sessionkey = hex("00000000000000000000000")
    connectDb()
    initDbSchema()
    install(DefaultHeaders)
    install(CallLogging)
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
            serializeNulls()
        }
        register(ContentType.Application.Json, GsonConverter())
    }

    install(Sessions) {
        //        cookie<MySession>("API_SESSION") {
//            cookie.duration = cookieDuration
//            cookie.path = "/api"
//            transform(SessionTransportTransformerMessageAuthentication(sessionkey))
//        }

        cookie<SessionToken>("API_TOKEN") {
            cookie.duration = cookieDuration
            cookie.path = "/api"
        }
    }
    install(SessionCache)
//    install(Authentication)
//    install(ContentNegotiation) {
//        register(ContentType.Application.Json, GsonConverter())
//    }
    install(Routing) {
        route("api"){
            route("v1"){
                route("public") {
                    userPublicService()
                    packsPublicService()
                }
                route("private") {
                    authentication {
                        jsonAthentification()
                    }
                    packsService()
                    walletService()
                    userPrivateService()
                }
            }
        }
    }
}
