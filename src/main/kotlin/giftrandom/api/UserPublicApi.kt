package giftrandom.api

import giftrandom.db.SessionService
import giftrandom.db.SessionToken
import giftrandom.db.UserService
import giftrandom.features.session
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.sessions.sessions
import io.ktor.sessions.set

data class RegisterDTO(var login: String, var email: String, var password: String) {
    fun validate(): Boolean {
        return !(login.isNullOrEmpty() || email.isNullOrEmpty() || password.isNullOrEmpty())
    }
}

data class LoginDTO(var login: String, var password: String)

fun Route.userPublicService() {
    route("user") {
        route("register") {
            post {
                var input: RegisterDTO? = call.receiveOrNull<RegisterDTO>()
                if (input != null && input.validate()) {
                    try {
                        val user = UserService.register(input.login, input.password, input.email, if ("admin".equals(input.login)) 100 else 0)
                        call.respond(success("ok"))
                    } catch (e: Exception) {
                        call.respond(errorRestMessage(e.message))
                    }
                } else {
                    call.respond(HttpStatusCode.BadRequest, errorRestMessage("input not valid"))
                }
            }
        }

        route("login") {
            post {
                try {
                    var ses = call.session
                    if (ses != null) {
                        call.respond(success("ok"))
                        //TODO REFACTOR TO AUTOLOGIN AFTER SIGNUP
                        return@post
                    }
                } catch (e: Exception) {
                }
                try {
                    var input: LoginDTO = call.receiveOrNull<LoginDTO>() ?: throw IllegalArgumentException("null input")
                    val session = SessionService.auth(input.login, input.password)
                    //call.sessions.set<MySession>(session)
                    call.sessions.set<SessionToken>(SessionToken(session.hash))


                    // var mySession:MySession = call.sessions.get<MySession>() ?: throw IllegalStateException("cant create session cookie")
                    call.respond(success("ok"))
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.BadRequest, errorRestMessage(e.message))
                }
            }
        }

        route("reset") {

        }
    }
}