package giftrandom.api

import io.ktor.http.Parameters
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.roundToInt

class Paginate(var current: Int, var perPage: Int, var pages: Int)

fun getPaginationIndex(page: Int, perPage: Int): Int {
    return max(0, page - 1) * perPage
}

fun getPaginationPages(total: Int, perPage: Int): Int {
    return kotlin.math.max(1, ceil(total.toDouble() / perPage.toDouble()).roundToInt())
}

fun getPaginationByParams(params: Parameters): Paginate {
    return Paginate(
            current = (params["page"] ?: "1").toInt(),
            perPage = (params["perPage"] ?: "10").toInt(),
            pages = 1
    )
}