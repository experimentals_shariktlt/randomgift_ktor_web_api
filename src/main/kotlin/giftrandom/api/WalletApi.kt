package giftrandom.api

import giftrandom.db.WalletDao
import giftrandom.features.session
import giftrandom.services.WalletService
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

data class AdminDepositDto(var amount: Long)
data class PromoDto(var code: String)
fun Route.walletService() {
    route("wallet") {
        route("balance") {
            get {
                var userId: Int = call.session!!.userId
                var amount = WalletDao.getAmount(userId)
                call.respond(success(amount))
            }
        }

        route("promo") {
            post {
                try {
                    var input = call.receive<PromoDto>()
                    var userId: Int = call.session!!.userId
                    if (call.session!!.group == 100) {
                        if (input.code.matches(Regex("^d\\d+"))) {
                            var amount = input.code.substring(1).toLong()
                            WalletService.deposit(userId, amount)
                        } else {
                            return@post call.respond(errorRestMessage("invalid code"))
                        }
                    } else {
                        return@post call.respond(errorRestMessage("invalid code"))
                    }

                    call.respond(WalletService.walletContainer("ok", userId))
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, errorRestMessage("error"))
                }
            }
        }

        route("admin_deposit") {
            post {
                try {
                    var ses = call.session!!
                    if (ses.group < 10) {
                        return@post call.respond(HttpStatusCode.NotFound)
                    }
                    var amount = call.receive<AdminDepositDto>().amount

                    if (amount == 0L) {
                        return@post call.respond(errorRestMessage("ammount should not equals zero"))
                    }
                    if (amount > 0) {
                        WalletService.deposit(ses.userId, amount)
                    } else {
                        WalletService.withdraw(ses.userId, amount)
                    }
                    call.respond(successWithBalance(true, ses.userId))
                } catch (e: Exception) {

                }
            }
        }
    }
}