package giftrandom.api

import giftrandom.dto.WalletContainer
import giftrandom.services.WalletService

open class RestResponse(var success: Boolean, var body: Any?)


fun success(body: Any?): RestResponse {
    return RestResponse(true, body)
}

fun successWithBalance(body: Any?, userId: Int): WalletContainer {
    var res = WalletService.walletContainer(body, userId)
    return res
}

fun errorRestMessage(body: Any?): RestResponse {
    return RestResponse(false, body)
}