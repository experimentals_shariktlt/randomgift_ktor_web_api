package giftrandom.api

import giftrandom.db.SessionService
import giftrandom.features.logout
import giftrandom.features.session
import giftrandom.services.UserProfileService
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

fun Route.userPrivateService() {
    route("user") {
        route("check") {
            get {
                try {
                    var session = call.session!!
                    call.respond(success(UserProfileService.getCurrentUserProfile(session)))
                } catch (e: IllegalAccessException) {
                    call.respond(HttpStatusCode.Unauthorized, errorRestMessage("unauthorized"))
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, errorRestMessage("internal error"))
                }
            }
        }

        route("logout") {
            post {
                SessionService.logout(call.session!!.hash)
                call.logout()
                call.respond(success("ok"))
            }
        }
    }
}