package giftrandom.api

import giftrandom.db.PackDAO
import giftrandom.db.toPack
import giftrandom.dto.Pack
import giftrandom.features.session
import giftrandom.services.GiftService
import io.ktor.application.call
import io.ktor.application.log
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.math.max

data class ListAddRequest(var name: String, var description: String, var image: String, val active: Boolean, val price: Int, val keyPrice: Int)

data class PackListWithPaginating(val packs: List<Pack>, val paginate: Paginate)

data class BuyPackageRequest(val packId: Int, val count: Int = 1)

data class SellPackageRequest(val packId: Int)

data class UnwrapPackageRequest(val packId: Int)

fun Route.packsPublicService() {
    route("packs") {
        route("list") {
            get {
                var params = call.parameters
                var paginate = getPaginationByParams(params)
                var currentIndex = getPaginationIndex(paginate.current, paginate.perPage)
                var list: List<PackDAO>? = null
                transaction({
                    var prepared = PackDAO.all().limit(paginate.perPage, currentIndex)
                    paginate.pages = getPaginationPages(prepared.count(), paginate.perPage)
                    list = prepared.toList()

                })
                var res = PackListWithPaginating(toPack(list!!), paginate)
                call.respond(success(res))
            }
        }
    }
}

fun Route.packsService() {
    route("packs") {

        route("user") {

            /**
             * List of user`s packages
             */
            route("list") {
                get {
                    var userId = call.session!!.userId
                    val userPacks = GiftService.getUserPacks(userId = userId)
                    call.respond(success(userPacks))
                }
            }

            /**
             * Buy specified pack
             */
            route("buy") {
                post {
                    var userId = call.session!!.userId
                    var req = call.receive<BuyPackageRequest>()
                    var packId = req.packId
                    var count: Int = max(req.count, 1)
                    try {
                        var giftPackage = GiftService.buyUserPack(userId, packId, count)
                        call.respond(successWithBalance(giftPackage, userId))
                    } catch (e: Exception) {
                        call.respond(errorRestMessage(e.message))
                    }
                }
            }

            /**
             * Sell specified pack, should be in WAIT or OPEN
             */
            route("sell") {
                post {
                    var userId = call.session!!.userId
                    var req = call.receive<SellPackageRequest>()
                    var packId = req.packId
                    try {
                        GiftService.sellUserPack(userId, packId)
                        call.respond(successWithBalance(true, userId))
                    } catch (e: Exception) {
                        call.application.log.error(e.message, e)
                        call.respond(errorRestMessage(e.message))
                    }
                }
            }

            route("unwrap") {
                post {
                    var userId = call.session!!.userId
                    var packId = 1
                    var req = call.receive<UnwrapPackageRequest>()
                    packId = req.packId
                    try {
                        call.respond(successWithBalance(GiftService.unwrapPack(userId, packId), userId))
                    } catch (e: Exception) {
                        call.application.log.error(e.message, e)
                        call.respond(errorRestMessage(e.message))
                    }
                }
            }
        }
    }
}


