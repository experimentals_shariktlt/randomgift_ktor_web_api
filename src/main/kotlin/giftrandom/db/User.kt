package giftrandom.db

import giftrandom.DB_COLLATE
import giftrandom.utils.Hashing
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object UserDb : IntIdTable("users") {
    val login = varchar("login", 256, DB_COLLATE)
    var email = varchar("email", 512, DB_COLLATE)
    var hash = varchar("hash", 60, DB_COLLATE)
    var group = integer("group")
    var created = datetime("created")
    var active = bool("active")
    var blocked = bool("blocked")
    var avatar = varchar("avatar", 1024)
}

/**
 * PackDAO(id: EntityID<Int>) : IntEntity(id) {
companion object : IntEntityClass<PackDAO>(PackDb)
 *
 */

class UserDao(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserDao>(UserDb)

    var login by UserDb.login
    var email by UserDb.email
    var hash by UserDb.hash
    var group by UserDb.group
    var created by UserDb.created
    var active by UserDb.active
    var blocked by UserDb.blocked
    var avatar by UserDb.avatar

}

class UserService {
    companion object Factory {
        val logger: Logger = LoggerFactory.getLogger(UserService.javaClass)

        fun loginUser(login: String, password: String): UserDao? {
            var user: UserDao = getUserByLogin(login) ?: throw IllegalArgumentException("user not found")
            if (!checkPassword(password, user.hash)) {
                throw IllegalArgumentException("invalid password")
            }
            if (user.blocked) {
                throw IllegalArgumentException("blocked account")
            }
            return user
        }

        private fun getUserByLogin(login: String): UserDao? {
            var user: UserDao? = null
            transaction {
                user = UserDao.find { UserDb.login.eq(login) or UserDb.email.eq(login) }.firstOrNull()
            }
            return user
        }

        fun getUserById(id: Int): UserDao? {
            var user: UserDao? = null
            transaction {
                user = UserDao.findById(id)
            }
            return user
        }

        fun register(loginIn: String, passwordIn: String, emailIn: String, groupIn: Int): UserDao? {
            var user: UserDao? = null
            try {
                transaction {
                    var existUser: UserDao? = UserDao.find { UserDb.login.eq(loginIn) or UserDb.email.eq(emailIn) }.firstOrNull()
                    if (existUser != null) {
                        throw IllegalArgumentException("user exist")
                    }
                    var userTransaction = UserDao.new {
                        login = loginIn
                        email = emailIn
                        hash = getHash(passwordIn)
                        group = groupIn
                        created = DateTime.now()
                        active = false
                        blocked = false
                        avatar = "na"
                    }
                    user = userTransaction
                }
            } catch (e: IllegalArgumentException) {
                throw e
            } catch (e: Exception) {
                logger.error(e.message)
                throw IllegalArgumentException("try again later")
            }
            return user
        }

        private fun getHash(password: String): String {
            var hashing = Hashing(12)
            return hashing.hash(password)
        }

        fun checkPassword(password: String, hash: String): Boolean {
            var hashing = Hashing(12)
            return hashing.verifyHash(password, hash)
        }

        fun checkBlock(userId: Int) {
            transaction {
                var user = UserDao.findById(userId)
                if (user == null) {
                    throw IllegalAccessError("not found")
                }
                if (user.blocked) {
                    throw IllegalAccessError("blocked user")
                }
            }
        }
    }
}