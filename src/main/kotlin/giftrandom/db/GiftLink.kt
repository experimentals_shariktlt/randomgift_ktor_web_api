package giftrandom.db

import giftrandom.DB_COLLATE
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object GiftLinkDb : IntIdTable("gift_link") {
    var pack = reference("pack", PackDb)
    var gift = reference("gift", GiftsDb)
    var lock = varchar("lock", 254, DB_COLLATE).uniqueIndex()
}

class GiftLinkDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<GiftLinkDAO>(GiftLinkDb)

    var pack by PackDAO referencedOn GiftLinkDb.pack
    var gift by GiftsDAO referencedOn GiftLinkDb.gift
    var lock by GiftLinkDb.lock

    fun buildLock() {
        lock = (pack.id.value.toString() + "_" + gift.id.value.toString())
    }

}