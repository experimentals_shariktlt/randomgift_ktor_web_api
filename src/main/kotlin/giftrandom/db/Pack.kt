package giftrandom.db

import giftrandom.DB_COLLATE
import giftrandom.dto.Pack
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import java.util.*

/**
 * Table definition
 */
object PackDb : IntIdTable("pack_info") {
    val name = varchar("name", length = 256, collate = DB_COLLATE)
    val descriptions = varchar("descriptions", length = 1024, collate = DB_COLLATE)
    val image = varchar("image", length = 1024)
    val active = bool("active").index().default(true)
    val price = long("price").default(30)
    val priceUnrwap = long("price_unwrap").default(70)
    val priceSell = long("price_sell").default(15)
}

/**
 * DAO
 */
class PackDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<PackDAO>(PackDb)

    var name by PackDb.name
    var descriptions by PackDb.descriptions
    var image by PackDb.image
    var active by PackDb.active
    var price by PackDb.price
    var priceUnwrap by PackDb.priceUnrwap
    var priceSell by PackDb.priceSell
    val gifts by GiftLinkDAO referrersOn GiftLinkDb.pack

    fun toPack():Pack{
        return Pack(id.value, name, descriptions, image, price, priceUnwrap, priceSell)
    }

}

fun toPack(list: List<PackDAO>): List<Pack> {
    val r = LinkedList<Pack>()
    for(p in list){
        r.add(p.toPack())
    }
    return r
}

