package giftrandom.db

import giftrandom.DB_COLLATE
import giftrandom.dto.GiftPackage
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object UserPackagesStatuses {
    val WAIT: String = "WAIT"
    val OPEN: String = "OPEN"
    val SOLD: String = "SOLD"

}

object UserPackagesDb : IntIdTable("user_packages") {
    val user = integer("user").index()
    val created = datetime("created").index()
    val opened = datetime("opened").index().nullable()
    val status = varchar("status", 64, collate = DB_COLLATE).index()
    val pack = reference("pack_id", PackDb)
    val gift = reference("gift_id", GiftsDb).nullable()
}

/**
 * DAO
 */
class UserPackagesDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserPackagesDAO>(UserPackagesDb)

    var user by UserPackagesDb.user
    var pack by PackDAO referencedOn UserPackagesDb.pack
    var gift by GiftsDAO optionalReferencedOn UserPackagesDb.gift
    var created by UserPackagesDb.created
    var opened by UserPackagesDb.opened
    var status by UserPackagesDb.status

    fun getGiftPackage(): GiftPackage {
        return GiftPackage(id.value, pack.toPack(), gift?.toGift(), status)
    }
}