package giftrandom.db

import giftrandom.DB_COLLATE
import giftrandom.utils.Hashing
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.util.*


data class MySession(var userId: Int, var login: String, var group: Int, var hash: String, var ttl: Long)
data class SessionToken(var token: String)

object SessionDb : IntIdTable("sessions") {
    var userId = integer("user_id")
    var login = varchar("login", 256, DB_COLLATE)
    var hash = varchar("hashkey", 60, DB_COLLATE)
    var ip = varchar("ip", 15, DB_COLLATE)
    var created = datetime("created")
    var lastRefresh = datetime("last_refresh")
    var closed = bool("closed").default(false)
}

class SessionDao(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<SessionDao>(SessionDb)

    var userId by SessionDb.userId
    var login by SessionDb.login
    var hash by SessionDb.hash
    var ip by SessionDb.ip
    var created by SessionDb.created
    var lastRefresh by SessionDb.lastRefresh
    var closed by SessionDb.closed

    fun toMySession(): MySession {
        var user: UserDao = getSessionUser() ?: throw IllegalArgumentException("cant find user from session")
        val ttl = Date().time
        return MySession(userId.toInt(), login, user.group, hash, ttl = ttl)
    }

    private fun getSessionUser(): UserDao? {
        var userDao: UserDao? = null
        transaction {
            userDao = UserDao.findById(userId)
        }
        return userDao
    }
}


class SessionService {
    companion object Factory {

        fun auth(loginIn: String, passwordIn: String): MySession {
            var user: UserDao = UserService.loginUser(loginIn, passwordIn)
                    ?: throw IllegalArgumentException("user not found")
            var sessionDao: SessionDao? = null
            val hashSeed: String = user.id.value.toString() + Date().toString() + Random().nextLong().toString()
            val ipUser = "unknown"
            transaction {
                sessionDao = SessionDao.new {
                    userId = user.id.value
                    login = user.login
                    hash = Hashing(5).hash(hashSeed)
                    ip = ipUser
                    created = DateTime.now()
                    lastRefresh = DateTime.now()
                }

            }
            return sessionDao!!.toMySession()
        }

        fun check(checkSession: MySession) {
            var sessionDao: SessionDao? = null
            var valid = false
            transaction {
                sessionDao = SessionDao.find { SessionDb.hash.eq(checkSession.hash) }.firstOrNull()
                if (sessionDao != null) {
                    if (sessionDao!!.login.equals(checkSession.login) && sessionDao!!.userId.equals(checkSession.userId) && !sessionDao!!.closed) {
                        valid = true
                        sessionDao!!.lastRefresh = DateTime.now()
                    }
                }
            }
            if (!valid) {
                throw IllegalAccessError("session not found")
            } else {
                UserService.checkBlock(sessionDao!!.userId)
            }
        }

        fun load(token: String): MySession {
            var session: MySession? = null
            transaction {
                var sessionDao = SessionDao.find { SessionDb.hash.eq(token) }.firstOrNull()
                        ?: throw IllegalAccessException("invalid api token")
                if (sessionDao.closed) throw IllegalStateException("closed session")
                session = sessionDao.toMySession()
            }
            return session!!
        }

        fun logout(token: String) {
            transaction {
                var sessionDao = SessionDao.find { SessionDb.hash.eq(token) }.firstOrNull()
                        ?: throw IllegalAccessException("invalid api token")
                sessionDao.closed = true
            }
        }
    }
}