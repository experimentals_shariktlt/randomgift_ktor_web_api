package giftrandom.db

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Table definition
 */
object WalletDb : Table("wallet") {
    val userRef = integer("user_ref").primaryKey()
    val amount = long("amount").index(false)
}

class WalletDao {
    companion object Factory {
        fun getAmount(userId: Int): Long {
            var amount: Long = -1
            transaction {
                val wallet = WalletDb.select { WalletDb.userRef.eq(userId) }.firstOrNull()
                if (wallet != null) {
                    amount = wallet[WalletDb.amount]
                }
            }
            return amount
        }

        private fun getWallet(userId: Int): ResultRow? {
            return WalletDb.select { WalletDb.userRef.eq(userId) }.firstOrNull()
        }

        fun withdraw(userId: Int, value: Long) {
            transaction {
                var wallet: ResultRow = getWallet(userId) ?: throw IllegalStateException("no wallet to withdraw")
                var newValue = Math.max(0, wallet[WalletDb.amount] - Math.abs(value))
                WalletDb.update({ WalletDb.userRef eq userId }) {
                    it.set(WalletDb.amount, newValue)
                }
            }
        }

        fun deposit(userId: Int, value: Long) {
            transaction {
                var wallet: ResultRow = getWallet(userId) ?: return@transaction createDeposit(userId, value)
                var newValue = Math.max(0, wallet[WalletDb.amount] + Math.abs(value))
                WalletDb.update({ WalletDb.userRef eq userId }) {
                    it.set(WalletDb.amount, newValue)
                }
            }

        }

        private fun createDeposit(userId: Int, value: Long) {
            transaction {
                WalletDb.insert {
                    it[userRef] = userId
                    it[amount] = value
                }
            }
        }
    }
}





