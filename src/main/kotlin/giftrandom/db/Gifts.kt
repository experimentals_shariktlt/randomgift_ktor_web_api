package giftrandom.db

import giftrandom.DB_COLLATE
import giftrandom.dto.Gift
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object GiftsDb : IntIdTable("gifts") {
    val name = varchar("name", length = 256, collate = DB_COLLATE)
    val description = varchar("description", length = 1024, collate = DB_COLLATE)
    val image = varchar("image", length = 1024)
    val url = varchar("url", length = 1024)
    val active = bool("active").index()
    val created = datetime("created").index()
    val updated = datetime("updated").index()
    val price = long("price").index()
    val realPrice = long("real_price")
    val grade = integer("grade").index().default(0)
}

class GiftsDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<GiftsDAO>(GiftsDb)

    var name by GiftsDb.name
    var description by GiftsDb.description
    var image by GiftsDb.image
    var url by GiftsDb.url
    var active by GiftsDb.active
    var created by GiftsDb.created
    var updated by GiftsDb.updated
    var price by GiftsDb.price
    var realprice by GiftsDb.realPrice
    val grade by GiftsDb.grade
    fun toGift(): Gift {
        return Gift(id.value, name, image, price)
    }
}

