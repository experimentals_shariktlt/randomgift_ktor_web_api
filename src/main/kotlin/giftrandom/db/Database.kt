package giftrandom.db

import com.mchange.v2.c3p0.ComboPooledDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction

fun connectDb() {
    val url = "jdbc:mysql://root:root@localhost:3306/gift_random?useUnicode=true&serverTimezone=UTC&nullNamePatternMatchesAll=true&&characterEncoding=utf-8"
    val driver = "com.mysql.cj.jdbc.Driver"
    val dataSource = ComboPooledDataSource()
    dataSource.driverClass = driver
    dataSource.jdbcUrl = url
    dataSource.maxPoolSize = 100
    dataSource.initialPoolSize = 20
    dataSource.minPoolSize = 10
    Database.connect(dataSource)
}

fun initDbSchema() {
    transaction {
        create(
                GiftsDb,
                PackDb,
                UserPackagesDb,
                WalletDb,
                UserActivityDb,
                GiftLinkDb,
                SessionDb,
                UserDb
        )
    }
}