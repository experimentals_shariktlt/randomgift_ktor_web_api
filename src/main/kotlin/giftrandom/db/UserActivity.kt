package giftrandom.db

import giftrandom.DB_COLLATE
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object UserActivityDb : IntIdTable("user_activity") {
    val user = integer("user").index()
    var ts = datetime("ts").index()
    var tag = varchar("tag", 64, DB_COLLATE)
    var msg = varchar("msg", 512, DB_COLLATE)
}

class UserActivityDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserActivityDAO>(UserActivityDb)

    var user by UserActivityDb.user
    var ts by UserActivityDb.ts
    var tag by UserActivityDb.tag
    var msg by UserActivityDb.msg
}

enum class UserActivityTag(val value: String) {
    BUY("buy"),
    SELL("sell"),
    DEPOSIT("deposit"),
    UNWRAP("unwrap")
}


class UserActivityService {
    companion object Factory {
        val LOGGER: Logger = LoggerFactory.getLogger(UserActivityService::class.java)

        fun log(userId: Int, tagActivity: UserActivityTag, msgActivity: String) {
            transaction {
                UserActivityDAO.new {
                    user = userId
                    ts = DateTime.now()
                    tag = tagActivity.value
                    msg = msgActivity
                }

            }
            LOGGER.info("[{}] by {}: {}", tagActivity, userId, msgActivity)
        }
    }
}