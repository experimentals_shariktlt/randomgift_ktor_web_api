package giftrandom.features

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import giftrandom.db.MySession
import giftrandom.db.SessionService
import giftrandom.db.SessionToken
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.ApplicationFeature
import io.ktor.application.call
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.util.AttributeKey
import java.util.concurrent.TimeUnit


class SessionCache(configuration: Configuration) {

    val cache: Cache<String, MySession> = CacheBuilder.newBuilder()
            .maximumSize(10000)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build<String, MySession>()

    class Configuration
    companion object Feature : ApplicationFeature<ApplicationCallPipeline, SessionCache.Configuration, SessionCache> {
        // create unique key for the feature
        override val key = AttributeKey<SessionCache>("SessionCache")

        // implement installation script
        override fun install(pipeline: ApplicationCallPipeline, configure: Configuration.() -> Unit): SessionCache {

            // run configuration script
            val configuration = SessionCache.Configuration().apply(configure)

            // create a feature
            val feature = SessionCache(configuration)

            pipeline.intercept(ApplicationCallPipeline.Infrastructure) {
                call.attributes.put(SessionKey, feature)
            }

            return feature
        }
    }

}


val ApplicationCall.session: MySession?
    get() {
        val sessionCache: SessionCache = attributes.getOrNull(SessionKey)
                ?: throw IllegalStateException("Sessions feature should be installed to use sessions")
        val sessionToken = sessions.get<SessionToken>() ?: throw IllegalAccessException("no api token")

        return sessionCache.cache.get(sessionToken.token) { SessionService.load(sessionToken.token) }
    }

fun ApplicationCall.logout() {
    val sessionCache: SessionCache = attributes.getOrNull(SessionKey)
            ?: throw IllegalStateException("Sessions feature should be installed to use sessions")
    sessionCache.cache.invalidate(this.session!!.hash)
}

private val SessionKey = AttributeKey<SessionCache>("SessionCacheKey")