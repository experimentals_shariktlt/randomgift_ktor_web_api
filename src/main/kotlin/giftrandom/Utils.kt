package giftrandom

import com.google.gson.Gson
import com.google.gson.GsonBuilder


val gson: Gson = GsonBuilder().setPrettyPrinting().create()
/**
 * Return json representation
 */
fun toJson(obj: Any): String {
    return gson.toJson(obj)
}


