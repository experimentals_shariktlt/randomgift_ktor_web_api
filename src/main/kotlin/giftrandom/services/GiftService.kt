package giftrandom.services

import giftrandom.db.*
import giftrandom.dto.GiftPackage
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.util.*
import kotlin.math.floor

class GiftService {
    companion object Factory {
        private val AVAILABLE_STATUSES: Iterable<String> = listOf(UserPackagesStatuses.WAIT, UserPackagesStatuses.OPEN)
        private val AVAILABLE_SELL_STATUSES: Iterable<String> = listOf(UserPackagesStatuses.WAIT, UserPackagesStatuses.OPEN)
        fun getUserPacks(userId: Int): List<GiftPackage> {
            var list = LinkedList<GiftPackage>()

            transaction {
                for (packDb in UserPackagesDAO.find { UserPackagesDb.user.eq(userId) and UserPackagesDb.status.inList(AVAILABLE_STATUSES) }) {
                    list.add(packDb.getGiftPackage())
                }
            }
            return list
        }

        fun buyUserPack(userId: Int, packId: Int, count: Int): MutableList<GiftPackage> {
            var buyedPackage: MutableList<GiftPackage> = mutableListOf()
            transaction {
                var packOrigin = PackDAO.findById(packId)
                if (packOrigin == null) {
                    throw IllegalArgumentException("No packs with packId " + packId)
                }
                var price = packOrigin.price * count
                val packageRawList = WalletService.transactionWithdraw(userId, price, fun(): MutableList<UserPackagesDAO> {
                    var list = mutableListOf<UserPackagesDAO>()
                    for(i in 1..count) {
                        list.add(UserPackagesDAO.new {
                            user = userId
                            pack = packOrigin
                            created = DateTime.now()
                            status = UserPackagesStatuses.WAIT
                        })
                    }

                    return list
                })
                var ids = mutableListOf<Int>()
                for(packageRaw in packageRawList!!){
                    val element = packageRaw.getGiftPackage()
                    buyedPackage.add(element)
                    ids.add(element.id)
                }

                UserActivityService.log(userId, UserActivityTag.BUY, String.format("[%d] %s @ %d x %d [%d]", packId, ids.joinToString(",", "{", "}"), packOrigin.price, count, price))
            }
            return buyedPackage
        }

        fun sellUserPack(userId: Int, packId: Int): Boolean {
            var res = false
            transaction {
                var packageToSell = UserPackagesDAO.find { UserPackagesDb.id.eq(packId) and UserPackagesDb.user.eq(userId) and UserPackagesDb.status.inList(AVAILABLE_SELL_STATUSES) }.firstOrNull()
                if (packageToSell == null || packageToSell.user != userId) {
                    throw IllegalArgumentException("Package not found")
                }
                var sellPrice = floor(packageToSell.pack.price * 0.6).toLong()

                if (packageToSell.gift != null) {
                    sellPrice = packageToSell.gift!!.price
                }
                WalletService.transactionDeposit(userId, sellPrice, fun(): Boolean {
                    transaction {
                        packageToSell.status = UserPackagesStatuses.SOLD
                    }
                    UserActivityService.log(userId, UserActivityTag.SELL, String.format("[%d] %d @ %d", packId, packageToSell.id.value, sellPrice))
                    return true
                })
            }
            return res
        }

        fun unwrapPack(userId: Int, packId: Int): GiftPackage? {
            var res: GiftPackage? = null
            transaction {
                var userPack = UserPackagesDAO.find { UserPackagesDb.id.eq(packId) and UserPackagesDb.id.eq(packId) }.firstOrNull()
                if (userPack == null) {
                    throw IllegalArgumentException("Package not found")
                }
                if (!userPack.status.equals(UserPackagesStatuses.WAIT)) {
                    throw IllegalArgumentException("Invalid package")
                } else {
                    userPack.gift = getRandomGiftForPackage(userId, userPack.pack)
                    res = userPack.getGiftPackage()
                }
            }
            return res
        }

        private fun getRandomGiftForPackage(userId: Int, pack: PackDAO): GiftsDAO? {
            var gifts = pack.gifts.toList()
            var resultGift = RandomizerService.chooseGift(userId, pack)
            return resultGift
        }
    }
}