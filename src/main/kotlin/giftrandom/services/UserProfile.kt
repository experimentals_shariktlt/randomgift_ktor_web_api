package giftrandom.services

import giftrandom.db.MySession
import giftrandom.db.UserService.Factory.getUserById
import giftrandom.db.WalletDao

data class UserProfile(var userId: Int, var login: String, var groupCode: Int, var email: String?, var balance: Long?, var avatar: String?) {
    constructor(userId: Int, login: String, groupCode: Int) : this(userId, login, groupCode, null, null, null)

}


class UserProfileService {
    companion object Factory {
        fun getCurrentUserProfile(session: MySession): UserProfile {
            return getUserProfile(session.userId)
        }

        private fun getUserProfile(userId: Int): UserProfile {
            val user = getUserById(userId) ?: throw IllegalStateException("user not found")
            val balance = Math.max(0, WalletDao.getAmount(userId))
            return UserProfile(user.id.value, user.login, user.group, user.email, balance, user.avatar)
        }
    }
}