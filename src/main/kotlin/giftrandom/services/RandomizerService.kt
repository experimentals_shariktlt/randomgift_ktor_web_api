package giftrandom.services

import giftrandom.db.GiftsDAO
import giftrandom.db.PackDAO
import java.util.*

data class CandidateGrade(var grade: Int, var gifts: MutableList<GiftsDAO>)

class RandomizerService {
    companion object Factory {
        fun chooseGift(userId: Int, pack: PackDAO): GiftsDAO? {
            var gift: GiftsDAO? = null
            var grades: MutableMap<Int, CandidateGrade> = HashMap<Int, CandidateGrade>()
            for (link in pack.gifts.notForUpdate().toList()) {
                var grade = link.gift.grade
                if (grades[grade] == null) {
                    grades.put(grade, CandidateGrade(grade, mutableListOf()))
                }
                grades[grade]!!.gifts.add(link.gift)
            }

            var maxGrade = 10
            var candidates: MutableList<GiftsDAO> = mutableListOf()
            grades.map {
                if (it.key <= maxGrade) {
                    candidates.addAll(it.value.gifts)
                }
            }
            candidates.shuffle()
            var seledcted = Random().nextInt(candidates.size)
            gift = candidates[seledcted]
            return gift
        }
    }
}