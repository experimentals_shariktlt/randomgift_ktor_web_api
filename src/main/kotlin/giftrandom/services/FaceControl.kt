package giftrandom.services

import giftrandom.api.errorRestMessage
import giftrandom.db.MySession
import giftrandom.db.SessionService
import giftrandom.features.session
import io.ktor.application.call
import io.ktor.auth.AuthenticationPipeline
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

data class FaceControlRequest(var login: String, var password: String)

fun AuthenticationPipeline.jsonAthentification() {
    intercept(AuthenticationPipeline.RequestAuthentication) { context ->
        var invalid = true
        try {
            var session: MySession? = call.session
            if (session != null) {
                SessionService.check(session)
                invalid = false
            }
        } catch (e: Exception) {
            invalid = true
        }
        if (invalid) {
            call.respond(HttpStatusCode.Unauthorized, errorRestMessage("need auth"))
        }
    }
}

private val formAuthenticationChallengeKey: Any = "FaceControl"