package giftrandom.services

import giftrandom.db.WalletDao
import giftrandom.dto.WalletContainer
import org.jetbrains.exposed.sql.transactions.transaction


class WalletService {
    companion object Factory {
        fun canBuy(userId: Int, price: Long): Boolean {
            val userAmount = WalletDao.getAmount(userId)
            return userAmount >= price
        }

        fun withdraw(userId: Int, value: Long) {
//            //todo add valdation to value
            WalletDao.withdraw(userId, value)
        }

        fun deposit(userId: Int, value: Long) {
            //todo add valdation to value
/*            transaction {
                var wallet = WalletDao.findById(userId)
                var newValue = (wallet?.amount ?: throw IllegalStateException("Cant get wallet")) + value
                wallet.amount = newValue

            }*/
            WalletDao.deposit(userId, value)
        }

        fun <T> transactionWithdraw(userId: Int, value: Long, op: () -> T): T? {
            var res: T? = null
            transaction {
                if (!canBuy(userId, value)) {
                    throw IllegalStateException("Not enough")
                }
                res = op()
                withdraw(userId, value)
            }
            return res
        }

        fun <T> transactionDeposit(userId: Int, value: Long, op: () -> T): T? {
            var res: T? = null
            transaction {
                res = op()
                deposit(userId, value)
            }
            return res
        }


        fun walletContainer(body: Any?, userId: Int): WalletContainer {
            return WalletContainer(WalletDao.getAmount(userId), body)
        }
    }
}